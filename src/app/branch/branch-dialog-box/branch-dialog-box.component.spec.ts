import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchDialogBoxComponent } from './branch-dialog-box.component';

describe('BranchDialogBoxComponent', () => {
  let component: BranchDialogBoxComponent;
  let fixture: ComponentFixture<BranchDialogBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchDialogBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchDialogBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

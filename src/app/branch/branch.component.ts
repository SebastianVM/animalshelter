import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { APIService } from '../api.service';
import { MatTable, MatDialog, MatSort, MatTableDataSource } from '@angular/material';
import{ BranchDialogBoxComponent } from './branch-dialog-box/branch-dialog-box.component';

@Component({
  selector: 'app-branch',
  templateUrl: './branch.component.html',
  styleUrls: ['./branch.component.css']
})
export class BranchComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['id', 'name', 'address', 'phone', 'employee number', 'action'];
  dataSource = new MatTableDataSource<any>();

  @ViewChild(MatTable,{static:true}) table: MatTable<any>;
  @ViewChild(MatSort, null) sort: MatSort;

  constructor(public dialog: MatDialog, private apiService: APIService) { }

  ngOnInit() {
    this.refreshDataSource();
  }

  ngAfterViewInit()
  {
    this.dataSource.sort = this.sort;
  }

  refreshDataSource()
  {
    this.apiService.getBranches()
    .subscribe((branches: []) => {
      console.log(branches);
      this.dataSource.data = branches;      
    });
  }

  openDialog(action,obj) {
    obj.action = action;
    const dialogRef = this.dialog.open(BranchDialogBoxComponent, {
      width: '400px',
      data:obj
    });
 
    dialogRef.afterClosed().subscribe(result => {
      if(result.event == 'Add'){
        this.addRowData(result.data);
      }else if(result.event == 'Update'){
        this.updateRowData(result.data);
      }else if(result.event == 'Delete'){
        //this.deleteRowData(result.data);
      }
    });
  }

  addRowData(row_obj)
  {
    var branch  = {
      branch_name: row_obj.branch_name,
      branch_address: row_obj.branch_address,
      branch_phone: row_obj.branch_phone,
      branch_employee_number: row_obj.branch_employee_number,
    };
    
    this.apiService.postBranch(branch).subscribe(responce => {
      console.log(responce);
    });

    this.refreshDataSource();
  }

  updateRowData(row_obj)
  {
    var branch  = {
      branch_id: row_obj.branch_id,
      branch_name: row_obj.branch_name,
      branch_address: row_obj.branch_address,
      branch_phone: row_obj.branch_phone,
      branch_employee_number: row_obj.branch_employee_number,
    };

    this.apiService.putBranch(branch).subscribe(responce => {
      console.log(responce);
    });

    this.refreshDataSource();
  }

}

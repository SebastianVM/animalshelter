import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatTable, MatDialog,MatTableDataSource, MatSort } from '@angular/material';
import { DialogBoxComponent } from './dialog-box/dialog-box.component';
import { APIService } from '../api.service';

/*export interface UsersData {
  name: string;
  id: number;
}
 
const ELEMENT_DATA: UsersData[] = [
  {id: 1560608769632, name: 'Artificial Intelligence'},
  {id: 1560608796014, name: 'Machine Learning'},
  {id: 1560608787815, name: 'Robotic Process Automation'},
  {id: 1560608805101, name: 'Blockchain'}
];*/

@Component({
  selector: 'app-animal',
  templateUrl: './animal.component.html',
  styleUrls: ['./animal.component.css']
})
export class AnimalComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['id', 'name', 'kind', 'Entry date','action'];
  dataSource = new MatTableDataSource<any>();

  @ViewChild(MatTable,{static:true}) table: MatTable<any>;
  @ViewChild(MatSort, null) sort: MatSort;

  constructor(public dialog: MatDialog, private apiService: APIService) { }

  ngOnInit() 
  {
    this.refreshDataSource();
  }

  ngAfterViewInit()
  {
    this.dataSource.sort = this.sort;
  }

  refreshDataSource()
  {
    this.apiService.getShelterAnimals()
    .subscribe((animals: []) => {
      console.log(animals);
      this.dataSource.data = animals;      
    });
  }

  openDialog(action,obj) {
    obj.action = action;
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '400px',
      data:obj
    });
 
    dialogRef.afterClosed().subscribe(result => {
      if(result.event == 'Add'){
        this.addRowData(result.data);
      }else if(result.event == 'Update'){
        this.updateRowData(result.data);
      }else if(result.event == 'Delete'){
        this.deleteRowData(result.data);
      }
    });
  }
 
  addRowData(row_obj)
  {
    var createdOn = new Date();
    var animal  = {
      animal_name: row_obj.animal_name,
      animal_kind: row_obj.animal_kind,
      animal_entry_date: row_obj.animal_entry_date,
      animal_age: row_obj.animal_age,
      animal_created_on: createdOn,
      branch_id: row_obj.branch_id,
      animal_status_id: row_obj.animal_status_id
    };
    
    this.apiService.postShelterAnimal(animal).subscribe(responce => {
      console.log(responce);
    });

    this.refreshDataSource();
  }

  updateRowData(row_obj){
    var animal  = {
      animal_id: row_obj.animal_id,
      animal_name: row_obj.animal_name,
      animal_kind: row_obj.animal_kind,
      animal_entry_date: row_obj.animal_entry_date,
      animal_age: row_obj.animal_age,
      animal_created_on: row_obj.animal_created_on,
      branch_id: row_obj.branch_id,
      animal_status_id: row_obj.animal_status_id
    };

    this.apiService.putShelterAnimal(animal).subscribe(responce => {
      console.log("SERVE RESPNCE CAPTURED....");
      console.log(responce);
      this.refreshDataSource();
    });
  }
  
  deleteRowData(row_obj){
    this.apiService.deleteShelterAnimal(row_obj.animal_id).subscribe(responce => {
      console.log(responce);
      this.refreshDataSource();
    });
  }

}

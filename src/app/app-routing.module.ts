import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './auth/login/login.component';
import { ShelterComponent } from './shelter/shelter.component';
import { AnimalComponent } from './animal/animal.component';
import { BranchComponent } from './branch/branch.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'shelter', component: ShelterComponent},
  { path: 'animals', component: AnimalComponent },
  { path: 'branches', component: BranchComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

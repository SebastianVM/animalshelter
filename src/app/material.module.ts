import { NgModule } from '@angular/core';
import {
    MatSidenavModule, 
    MatListModule, 
    MatIconModule, 
    MatToolbarModule, 
    MatButtonModule, 
    MatCardModule, 
    MatFormFieldModule, 
    MatInputModule,
    MatTableModule, 
    MatDialogModule, 
    MatDatepickerModule, 
    MatNativeDateModule, 
    MatSortModule 
} from '@angular/material';

@NgModule({
    imports: [
        MatSidenavModule,
        MatListModule,
        MatIconModule,
        MatToolbarModule,
        MatButtonModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatTableModule,
        MatDialogModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatSortModule
    ],
    exports: [
        MatSidenavModule,
        MatListModule,
        MatIconModule,
        MatToolbarModule,
        MatButtonModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatTableModule,
        MatDialogModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatSortModule
    ]
})
export class MaterialModule{}
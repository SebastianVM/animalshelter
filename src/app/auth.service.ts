import { Subject } from 'rxjs';

export class AuthService
{
    authChange = new Subject<boolean>();

    loginUser()
    {
        this.authChange.next(true);
    }

    logoutUser()
    {
        this.authChange.next(false);
    }
}
import { Component, OnInit, Output, EventEmitter, OnDestroy} from '@angular/core';
import { AuthService } from 'src/app/auth.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  @Output() sidenavToggle = new EventEmitter<void>();
  isAuth: boolean;
  authSubsciption: Subscription;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.authSubsciption = this.authService.authChange.subscribe(data => {
      this.isAuth = data;
    });
  }

  ngOnDestroy()
  {
    this.authSubsciption.unsubscribe();
  }

  onToggleSidenav()
  {
    this.sidenavToggle.emit();
  }

  onLogout()
  {
    this.authService.logoutUser();
    this.router.navigate(['/']);
  }

}

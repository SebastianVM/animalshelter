import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { LoginComponent } from './auth/login/login.component';
import { BranchComponent } from './branch/branch.component';
import { NewBranchComponent } from './branch/new-branch/new-branch.component';
import { UpdateBranchComponent } from './branch/update-branch/update-branch.component';
import { AnimalComponent } from './animal/animal.component';
import { NewAnimalComponent } from './animal/new-animal/new-animal.component';
import { UpdateAnimalComponent } from './animal/update-animal/update-animal.component';
import { ShelterComponent } from './shelter/shelter.component';
import { HeaderComponent } from './navigation/header/header.component';
import { SidenavComponent } from './navigation/sidenav/sidenav.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { APIService } from './api.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogBoxComponent } from './animal/dialog-box/dialog-box.component';
import { AuthService } from './auth.service';
import { BranchDialogBoxComponent } from './branch/branch-dialog-box/branch-dialog-box.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    BranchComponent,
    NewBranchComponent,
    UpdateBranchComponent,
    AnimalComponent,
    NewAnimalComponent,
    UpdateAnimalComponent,
    ShelterComponent,
    HeaderComponent,
    SidenavComponent,
    DialogBoxComponent,
    BranchDialogBoxComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  entryComponents: [
    DialogBoxComponent,
    BranchDialogBoxComponent
  ],
  providers: [
    APIService,
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
